#include "ui.h"
#include "types.h"
#include "window.h"
#include "color.h"
#include "string_match.h"

#include <functional>
#include <iostream>
#include <string>

#include <Windows.h>
#include <Shlwapi.h>

#ifdef  USE_BOOST_LIB
#include <boost\algorithm\string.hpp>
#include <boost\lexical_cast.hpp>
#endif //  USE_BOOST_LIB

#include <leptonica\allheaders.h>
#include <tesseract\baseapi.h>


namespace cotli {

ui::ui()
	: _maxCoord(65535),
	_systemX(GetSystemMetrics(SM_CXSCREEN)),
	_systemY(GetSystemMetrics(SM_CYSCREEN)),
	_lowestVisibleCrusader(0),
	_lowestCampaignVisible(0),
	_window(NULL),
	_tesseractAPI(nullptr)
{
	_window = ::cotli::window::RetrieveWindow(L"Crusaders of The Lost Idols");

	if (_window == NULL)
	{
		// TODO : throw exception - window not found
	}

	_tesseractAPI = new tesseract::TessBaseAPI();

	if (_tesseractAPI->Init(NULL, "eng")) {
		// TODO : throw exception - Could not initialize tesseract
	}
}

ui::~ui()
{
	delete _tesseractAPI;
	_tesseractAPI = nullptr;
}

bool ui::foreground()
{
	HWND topWindow = GetForegroundWindow();
	if (topWindow != _window)
	{
		::cotli::window::ForceForegroundWindow(_window);
		Sleep(1000);
		topWindow = GetForegroundWindow();
	}
	return topWindow == _window;
}

#ifndef USE_BOOST_LIB
std::string trim(const std::string& in)
{
	size_t first = in.find_first_not_of(" \n\t");
	size_t last = in.find_last_not_of(" \n\t");
	// TODO : check first/last before substr
	return in.substr(first, (last - first + 1));
}

uint32_t extract_level(const std::string& ocrText)
{
	uint32_t level = 0;

	if (ocrText.compare(0, 3, "Lvl") == 0)
	{
		std::string trimmed = trim(ocrText.substr(3));
		std::istringstream iss(trimmed);
		iss >> level;
	}
	return level;
}
#endif // !USE_BOOST_LIB

bool ui::is_in_game()
{
	RECT rect{ 270, 60, 565, 100 };
	std::string ocrText = extract_text(rect);

	return !(::boost::iequals(ocrText, "Campaign Objectives"));
}

bool ui::is_event_period()
{
	RECT area{ 40,235,170,263 };
	std::string ocrText = extract_text(area);

	float ratio_threshold = 0.65f;

	return !string_match::is_near_of(ocrText, "World's Wake", ratio_threshold);
}

void ui::reset_crusader_selector()
{
	for (size_t i = 0; i < 7; ++i)
	{
		clickOnLeftSlider();
		Sleep(50);
	}
	_lowestVisibleCrusader = 0;
	Sleep(300);
}

ui::visibility ui::get_crusader_visibility(index_t idx)
{
	ui::visibility visibility = ui::visibility::visible;

	if (idx < _lowestVisibleCrusader)
		visibility = ui::visibility::lower;
	else if (idx >= (_lowestVisibleCrusader + 6))
		visibility = ui::visibility::higher;

	return visibility;
}

ui::visibility ui::get_campaign_visibility(index_t idx, bool is_event_period)
{
	ui::visibility visibility = ui::visibility::visible;

	if (idx < _lowestCampaignVisible)
		visibility = ui::visibility::lower;
	else if (idx >= (_lowestCampaignVisible + (is_event_period ? 2 : 4)))
		visibility = ui::visibility::higher;

	return visibility;
}

bool ui::is_ability_locked(Pix * img)
{
	color::RGB locked{ 133, 50, 19 };
	float global_threshold = 0.8f;
	float pixel_threshold = 2.0f;

	auto predicate = [pixel_threshold, &locked](const color::RGB& extracted)->bool {
		return (extracted == locked) || color::are_similar(locked, extracted, pixel_threshold);
	};
	return iterate_through_pixels(img, predicate, global_threshold);

}

bool ui::is_ability_not_activable(Pix * img)
{
	float global_threshold = 0.8f;

	return iterate_through_pixels(img, color::near_of_grey, global_threshold);
}

bool ui::is_ability_activable(Pix * img)
{
	float global_threshold = 0.8f;

	return iterate_through_pixels(img, color::near_of_greenyellow, global_threshold);
}

bool ui::is_slider_enable(const RECT& area)
{
	POINT outZone{ 55, 583 };
	move(outZone);
	Sleep(200);

	Pix* img = take_screenshot(area);

	color::RGB active{ 255, 177, 3 };
	float global_threshold = 0.8f;
	float pixel_threshold = 2.0f;

	auto predicate = [pixel_threshold, &active](const color::RGB& extracted)->bool {
		return (extracted == active) || color::are_similar(active, extracted, pixel_threshold);
	};
	bool retval = iterate_through_pixels(img, predicate, global_threshold);

	pixDestroy(&img);
	return retval;
}

void ui::unlock_campaign_selector(bool is_event_period)
{
	POINT offset{ 0, is_event_period ? 145 : 0 };

	POINT p{ 585,195 + offset.y };
	POINT q{ 585,250 + offset.y };

	toScreenCoords(p);
	toScreenCoords(q);

	INPUT mip;
	ZeroMemory(&mip, sizeof(mip));
	mip.type = INPUT_MOUSE;
	mip.mi.dx = p.x;
	mip.mi.dy = p.y;
	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);

	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);

	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);

	mip.mi.dx = q.x;
	mip.mi.dy = q.y;
	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);

	mip.mi.dx = p.x;
	mip.mi.dy = p.y;
	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);

	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);
}

bool ui::slideLeft()
{
	bool ret = false;
	if (_lowestVisibleCrusader > 0)
	{
		RECT area{ 13,582,18,589 };
		if (is_slider_enable(area))
		{
			clickOnLeftSlider();
			Sleep(250);
			_lowestVisibleCrusader -= 2;
			ret = true;
		}
	}
	return ret;
}

bool ui::slideRight()
{
	bool ret = false;
	if (_lowestVisibleCrusader < 14)
	{
		RECT area{ 981,581,986,588 };
		if (is_slider_enable(area))
		{
			clickOnRightSlider();
			Sleep(250);
			_lowestVisibleCrusader += 2;
			ret = true;
		}
	}
	return ret;
}

uint32_t ui::level_up(index_t crusader_idx, uint32_t max)
{
	uint32_t up = 0;

	if (select_crusader_index(crusader_idx))
	{
		RECT area{ 255,506,320,522 };
		offset_area(area, crusader_idx);

		bool try_again = true;
		while (try_again)
		{
			INPUT ip;
			ZeroMemory(&ip, sizeof(ip));
			ip.type = INPUT_KEYBOARD;
			ip.ki.wVk = VK_CONTROL;
			SendInput(1, &ip, sizeof(INPUT));
			Sleep(250);

			std::string extracted = extract_text(area);

			// KEYEVENTF_KEYUP for key release
			ip.ki.dwFlags = KEYEVENTF_KEYUP;
			SendInput(1, &ip, sizeof(INPUT));
			Sleep(50);

			std::cout << "possible up : " << extracted << std::endl;

			if (::boost::istarts_with(extracted, "x"))
			{
				extracted = extracted.substr(1);
				::boost::algorithm::trim(extracted);
				uint32_t possible_up = 0;

				try {
					possible_up = ::boost::lexical_cast<uint32_t>(extracted);
				}
				catch (const ::boost::bad_lexical_cast& e) {
					try_again = false;
					std::cout << e.what() << std::endl;
				}

				if ((possible_up == 100) && (max > 100))
				{
					POINT pt{ 285,525 };
					offset_point(pt, crusader_idx);

					level_up_100(pt);
					up += 100;
					possible_up = 0;
					max -= 100;
				}
				else if (possible_up > 25)
				{
					try_again = false;
					while ((possible_up > 25) && (max > 0))
					{
						POINT pt{ 285,525 };
						offset_point(pt, crusader_idx);

						level_up_25(pt);
						up += 25;
						possible_up -= 25;
						max -= 25;
					}
				}
				else
				{
					try_again = false;
				}
			}
			else
			{
				try_again = false;
			}
		}
	}

	return up;
}

void ui::level_up_25(POINT& point)
{
	INPUT input[2];
	ZeroMemory(&input, sizeof(input));

	input[0].type = INPUT_KEYBOARD;
	input[0].ki.wVk = VK_CONTROL;

	input[1].type = INPUT_KEYBOARD;
	input[1].ki.wVk = VK_SHIFT;

	SendInput(2, input, sizeof(INPUT));
	Sleep(250);
	click(point);

	input[0].ki.dwFlags = KEYEVENTF_KEYUP;
	input[1].ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(2, input, sizeof(INPUT));
	Sleep(50);
}

void ui::level_up_100(POINT& point)
{
	INPUT ip;
	ZeroMemory(&ip, sizeof(ip));
	ip.type = INPUT_KEYBOARD;
	ip.ki.wVk = VK_CONTROL;
	SendInput(1, &ip, sizeof(INPUT));
	Sleep(250);

	click(point);

	// KEYEVENTF_KEYUP for key release
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
	Sleep(50);
}

bool ui::iterate_through_pixels(Pix * img, std::function<bool(const color::RGB&)> predicate, float threshold)
{
	unsigned int count_valid = 0;

	for (l_uint32 h = 0; h < img->h; ++h)
	{
		for (l_uint32 w = 0; w < img->w; ++w)
		{
			color::RGB extractedColor;
			if (pixGetRGBPixel(img, w, h, &extractedColor.red, &extractedColor.green, &extractedColor.blue) == 0)
			{
				if (predicate(extractedColor))
				{
					count_valid++;
				}
			}
		}
	}
	float ratio = static_cast<float>(count_valid) / (img->h*img->w);
	return ratio >= threshold;
}

bool ui::wait_text_present(const RECT& area, const std::string & expected)
{
	bool is_present = false;

	while (!is_present)
	{
		// TODO : add a timeout ?
		std::string extracted = extract_text(area);
		if (::boost::istarts_with(extracted, expected))
		{
			is_present = true;
		}
		Sleep(5000);
	}

	return is_present;
}

uint32_t ui::retrieve_level(index_t crusaderIdx)
{
	uint32_t level = 0;
	if (select_crusader_index(crusaderIdx))
	{
		RECT rect{ 255, 550, 320, 560 };
		offset_area(rect, crusaderIdx);

		std::string ocrText = extract_text(rect);

#ifdef USE_BOOST_LIB
		try {
			if (::boost::starts_with(ocrText, "Lvl"))
			{
				ocrText = ocrText.substr(3);
				::boost::algorithm::trim(ocrText);
				level = ::boost::lexical_cast<uint32_t>(ocrText);
			}
		}
		catch (const ::boost::bad_lexical_cast& e) {
			// do anything, level is already set to 0
			std::cout << e.what() << std::endl;
		}
#else
		level = extract_level(ocrText);
#endif // USE_BOOST_LIB
	}
	return level;
}

ability_state ui::retrieve_ability(index_t crusaderIdx, index_t abilityIdx)
{
	ability_state state = ability_state::active;

	if (select_crusader_index(crusaderIdx))
	{
		RECT area{ 38,574,54,576 };
		offset_ability_area(area, crusaderIdx, abilityIdx);

		Pix* img = take_screenshot(area);

		if (is_ability_locked(img))
		{
			state = ability_state::locked;
		}
		else if (is_ability_not_activable(img))
		{
			state = ability_state::not_activable;
		}
		else if (is_ability_activable(img))
		{
			state = ability_state::activable;
		}

		pixDestroy(&img);
	}
	return state;
}

bool ui::select_crusader_index(index_t idx)
{
	bool isSucceeded = true;
	ui::visibility visibility = get_crusader_visibility(idx);

	if (visibility != ui::visibility::visible)
	{
		std::function<bool()> slide;
		if (visibility == ui::visibility::lower)
		{
			slide = std::bind(&ui::slideLeft, this);
		}
		else // (visibility == CrusaderVisibility::higher)
		{
			slide = std::bind(&ui::slideRight, this);
		}

		bool slideSucceed = true;
		do {
			slideSucceed = slide();
			visibility = get_crusader_visibility(idx);
		} while (slideSucceed && visibility != ui::visibility::visible);

		isSucceeded = (visibility == ui::visibility::visible);
		Sleep(500); // Need to wait slide ended before doing something else
	}

	return isSucceeded;
}

bool ui::select_crusader(index_t idx, const std::string & name)
{
	bool isSelected = false;

	if (select_crusader_index(idx))
	{
		float ratio_threshold = 0.65f;
		RECT area{ 80,505,210,520 };
		offset_area(area, idx);

		std::string ocrText = extract_text(area);

		if (string_match::is_near_of(ocrText, name, ratio_threshold))
			isSelected = true;
		else if (is_swappable_crusader(idx))
		{
			isSelected = swap_active_crusader(idx, name);
		}
	}

	return isSelected;
}

bool ui::is_swappable_crusader(index_t idx)
{
	bool swappable = false;
	if (select_crusader_index(idx))
	{
		RECT area{ 212,510,216,514 };
		offset_area(area, idx);

		Pix* img = take_screenshot(area);

		float global_threshold = 0.8f;
		swappable = iterate_through_pixels(img, color::near_of_greenyellow, global_threshold);
		pixDestroy(&img);
	}

	return swappable;
}

bool ui::swap_active_crusader(index_t idx, const std::string & name)
{
	POINT swap_list{ 225,510 };
	offset_point(swap_list, idx);
	click(swap_list);

	std::string previous;
	bool finded = false;
	bool same = false;

	do {
		RECT area{ 205,375,295,407 };
		offset_area(area, idx);

		std::string extracted = extract_text(area);
		std::replace(extracted.begin(), extracted.end(), '\n', ' ');

		float ratio_threshold = 0.65f;

		if (string_match::is_near_of(extracted, name, ratio_threshold))
		{
			finded = true;
			POINT swap{ 195, 450 };
			offset_point(swap, idx);
			click(swap);
		}
		else if (extracted == previous)
		{
			same = true;
			POINT close{ 277, 342 };
			offset_point(close, idx);
			click(close);
		}
		else
		{
			previous = extracted;
			POINT down{ 224, 482 };
			offset_point(down, idx);
			click(down);
		}
	} while (!finded && !same);


	return finded;
}

void ui::toScreenCoords(POINT& pt)
{
	MapWindowPoints(_window, NULL, &pt, 1);
	pt.x = pt.x * _maxCoord / _systemX;
	pt.y = pt.y * _maxCoord / _systemY;
}

void ui::move(POINT& pt)
{
	toScreenCoords(pt);

	INPUT mip;
	ZeroMemory(&mip, sizeof(mip));
	mip.type = INPUT_MOUSE;
	mip.mi.dx = pt.x;
	mip.mi.dy = pt.y;
	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);

	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);
}

void ui::click(POINT& pt)
{
	toScreenCoords(pt);

	INPUT mip;
	ZeroMemory(&mip, sizeof(mip));
	mip.type = INPUT_MOUSE;
	mip.mi.dx = pt.x;
	mip.mi.dy = pt.y;
	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);

	SendInput(1, &mip, sizeof(INPUT));
	Sleep(100);

	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(100);

	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(100);
}

void ui::offset_area(RECT& area, index_t crusader_idx)
{
	index_t offset = crusader_idx - _lowestVisibleCrusader;
	// 315	: horizontal offset between two crusader
	// 86	: vertical offset between two crusader
	area.left += ((offset / 2) * 315);
	area.right += ((offset / 2) * 315);
	area.top += (offset % 2) * 86;
	area.bottom += (offset % 2) * 86;
}

void ui::offset_ability_area(RECT& area, index_t crusader_idx, index_t ability_idx)
{
	index_t offset = crusader_idx - _lowestVisibleCrusader;
	// 315	: horizontal offset between two crusaders
	// 29	: horizontal offset between two ability
	// 86	: vertical offset between two crusader
	area.left += ((offset / 2) * 315) + (ability_idx * 29);
	area.right += ((offset / 2) * 315) + (ability_idx * 29);
	area.top += (offset % 2) * 86;
	area.bottom += (offset % 2) * 86;
}

void ui::offset_point(POINT& pt, index_t crusader_idx)
{
	index_t offset = crusader_idx - _lowestVisibleCrusader;
	// 315	: horizontal offset between two crusader
	// 86	: vertical offset between two crusader
	pt.x += (offset / 2) * 315;
	pt.y += (offset % 2) * 86;
}

void ui::offset_ability_point(POINT& pt, index_t crusader_idx, index_t ability_idx)
{
	index_t offset = crusader_idx - _lowestVisibleCrusader;
	// 315	: horizontal offset between two crusaders
	// 29	: horizontal offset between two ability
	// 86	: vertical offset between two crusader
	pt.x += ((offset / 2) * 315) + (ability_idx * 29);
	pt.y += (offset % 2) * 86;
}

void ui::clickOnLeftSlider()
{
	POINT pt{ 15, 585 };
	click(pt);
}

void ui::clickOnRightSlider()
{
	POINT pt{ 985, 585 };
	click(pt);
}

void ui::click_on_ability(index_t crusaderIdx, index_t abilityIdx)
{
	if (select_crusader_index(crusaderIdx))
	{
		POINT pt{ 45, 565 }; // coord for first ability of first displayed crusader
		offset_ability_point(pt, crusaderIdx, abilityIdx);
		click(pt);
	}
}

void ui::putCrusaderOnField(index_t crusaderIdx, POINT& slotCoord)
{
	if (select_crusader_index(crusaderIdx))
	{
		POINT pt{ 55, 525 };
		offset_point(pt, crusaderIdx);

		toScreenCoords(pt);
		toScreenCoords(slotCoord);

		INPUT mip;
		ZeroMemory(&mip, sizeof(mip));
		mip.type = INPUT_MOUSE;
		mip.mi.dx = pt.x;
		mip.mi.dy = pt.y;
		mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);

		SendInput(1, &mip, sizeof(INPUT));
		Sleep(50);

		mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN);
		SendInput(1, &mip, sizeof(INPUT));
		Sleep(50);

		mip.mi.dx = slotCoord.x;
		mip.mi.dy = slotCoord.y;
		mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);
		SendInput(1, &mip, sizeof(INPUT));
		Sleep(50);

		mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP);
		SendInput(1, &mip, sizeof(INPUT));
		Sleep(50);
	}
}

void ui::free_slot(POINT& slotCoord)
{
	POINT outZone{ 55, 583 };

	toScreenCoords(outZone);
	toScreenCoords(slotCoord);

	INPUT mip;
	ZeroMemory(&mip, sizeof(mip));
	mip.type = INPUT_MOUSE;
	mip.mi.dx = slotCoord.x;
	mip.mi.dy = slotCoord.y;
	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);

	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);

	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTDOWN);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);

	mip.mi.dx += 500; // TODO : find a better solution ?
	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);

	mip.mi.dx = outZone.x;
	mip.mi.dy = outZone.y;
	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);

	mip.mi.dwFlags = (MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_LEFTUP);
	SendInput(1, &mip, sizeof(INPUT));
	Sleep(50);
}

void ui::switch_auto_progression()
{
	INPUT ip;
	ZeroMemory(&ip, sizeof(ip));
	ip.type = INPUT_KEYBOARD;
	ip.ki.wVk = 0x47; // G key

	// Send the keyboard event to the specified window
	SendInput(1, &ip, sizeof(INPUT));
	Sleep(50);

	// KEYEVENTF_KEYUP for key release
	ip.ki.dwFlags = KEYEVENTF_KEYUP;
	SendInput(1, &ip, sizeof(INPUT));
	Sleep(50);
}

bool ui::is_enable_auto_progression()
{
	POINT pt{ 317,110 };
	click(pt);
	Sleep(2000);

	RECT area{ 208,335,211,338 };
	Pix* img = take_screenshot(area);

	color::RGB auto_progress{ 232, 205, 197 };
	float global_threshold = 0.8f;
	float pixel_threshold = 2.0f;

	auto predicate = [pixel_threshold, &auto_progress](const color::RGB& extracted)->bool {
		return (extracted == auto_progress) || color::are_similar(auto_progress, extracted, pixel_threshold);
	};
	bool retval = iterate_through_pixels(img, predicate, global_threshold);

	pixDestroy(&img);

	POINT close{ 735,195 };
	click(close);
	Sleep(2000);

	return retval;
}

void ui::disable_auto_progression()
{
	if (is_enable_auto_progression())
	{
		switch_auto_progression();
	}
}

void ui::enable_auto_progression()
{
	if (!is_enable_auto_progression())
	{
		switch_auto_progression();
	}
}

void ui::reset_world(index_t crusader_idx, index_t ability_idx)
{
	click_on_ability(crusader_idx, ability_idx);

	RECT reset_text{ 380,507,440,529 };
	if (!wait_text_present(reset_text, "Reset"))
		return; // TODO : throw exception ?

	POINT reset{ 405, 520 };
	click(reset);

	RECT shiny_text{ 230,92,790,141 };
	if (!wait_text_present(shiny_text, "Press the shiny button"))
		return; // TODO : throw exception

	POINT shiny_button{ 509, 536 };
	click(shiny_button);

	RECT continue_text{ 470,540,550,560 };
	if (!wait_text_present(continue_text, "Continue"))
		return;

	POINT continue_button{ 509,553 };
	click(continue_button);
}

void ui::start_objective(index_t campaign_idx, index_t objective_idx)
{
	bool is_event_period = this->is_event_period();
	unlock_campaign_selector(is_event_period);
	POINT offset{ 0, is_event_period ? 145 : 0 };

	ui::visibility campaign_visibility = get_campaign_visibility(campaign_idx, is_event_period);

	while (campaign_visibility != ui::visibility::visible)
	{
		POINT pt{ 584,0 };

		if (campaign_visibility == ui::visibility::lower)
		{
			pt.y = 164 + offset.y;
			_lowestCampaignVisible--;
		}
		else // campaign_visibility == ui::visibility::higher
		{
			pt.y = 606;
			_lowestCampaignVisible++;
		}

		click(pt);
		Sleep(500);

		campaign_visibility = get_campaign_visibility(campaign_idx, is_event_period);
	}

	POINT obj{ 284,184 };
	POINT obj_offset{ 51,51 };

	obj.x += (objective_idx / 2) * obj_offset.x;
	obj.y += (objective_idx % 2) * obj_offset.y;
	click(obj);

	POINT start{ 790,550 };
	click(start);

	while (!is_in_game())
	{
		Sleep(1000);
	}
}

std::string ui::extract_text(const RECT& area)
{
	Pix* img = take_screenshot(area);
	Pix* gray = pixConvertRGBToGray(img, 0.f, 0.f, 0.f);
	Pix* threshold = pixScaleGray4xLIThresh(gray, 190);
	Pix* unpack = pixUnpackBinary(threshold, 8, 0);

	_tesseractAPI->SetImage(unpack);
	char* outText = _tesseractAPI->GetUTF8Text();
	std::string ocrText = std::string(outText);

#ifdef USE_BOOST_LIB
	::boost::algorithm::trim(ocrText);
#else
	ocrText = trim(ocrText);
#endif	

	pixDestroy(&unpack);
	pixDestroy(&threshold);
	pixDestroy(&gray);
	pixDestroy(&img);

	return ocrText;
}

Pix* ui::take_screenshot(const RECT& rect)
{
	HDC hdcWindow = GetDC(_window);
	HDC hdcMemDC = CreateCompatibleDC(hdcWindow);

	int width = rect.right - rect.left;
	int height = rect.bottom - rect.top;

	HBITMAP hbmScreen = CreateCompatibleBitmap(hdcWindow, width, height);
	SelectObject(hdcMemDC, hbmScreen);
	BitBlt(hdcMemDC, 0, 0, width, height, hdcWindow, rect.left, rect.top, SRCCOPY);

	BITMAP bmpScreen;
	GetObject(hbmScreen, sizeof(BITMAP), &bmpScreen);

	BITMAPINFOHEADER   bi;
	bi.biSize = sizeof(BITMAPINFOHEADER);
	bi.biWidth = bmpScreen.bmWidth;
	bi.biHeight = bmpScreen.bmHeight;
	bi.biPlanes = 1;
	bi.biBitCount = 32;
	bi.biCompression = BI_RGB;
	bi.biSizeImage = 0;
	bi.biXPelsPerMeter = 0;
	bi.biYPelsPerMeter = 0;
	bi.biClrUsed = 0;
	bi.biClrImportant = 0;

	DWORD dwBmpSize = ((bmpScreen.bmWidth * bi.biBitCount + 31) / 32) * 4 * bmpScreen.bmHeight;

	// Starting with 32-bit Windows, GlobalAlloc and LocalAlloc are implemented as wrapper functions that 
	// call HeapAlloc using a handle to the process's default heap. Therefore, GlobalAlloc and LocalAlloc 
	// have greater overhead than HeapAlloc.
	HANDLE hDIB = GlobalAlloc(GHND, dwBmpSize);
	char *lpbitmap = (char *)GlobalLock(hDIB);

	// Gets the "bits" from the bitmap and copies them into a buffer 
	// which is pointed to by lpbitmap.
	GetDIBits(hdcWindow, hbmScreen, 0, (UINT)bmpScreen.bmHeight, lpbitmap, (BITMAPINFO *)&bi, DIB_RGB_COLORS);

	// Transfert to Leptonica
	Pix* image = pixCreate(bi.biWidth, bi.biHeight, bi.biBitCount);
	pixSetInputFormat(image, IFF_BMP);

	char* writeCursor = (char*)image->data;
	char* line = lpbitmap + ((bi.biHeight - 1) * bi.biWidth * 4);

	for (int h = bi.biHeight - 1; h >= 0; h--)
	{
		char* t = line;
		for (int w = 0; w < bi.biWidth; ++w)
		{
			// RGBA -> ARGB
			*writeCursor++ = *(t + 3);
			*writeCursor++ = *(t + 0);
			*writeCursor++ = *(t + 1);
			*writeCursor++ = *(t + 2);

			t += 4;
		}

		line -= (bi.biWidth * 4);
	}

	//Unlock and Free the DIB from the heap
	GlobalUnlock(hDIB);
	GlobalFree(hDIB);

	DeleteObject(hbmScreen);
	DeleteObject(hdcMemDC);
	ReleaseDC(_window, hdcWindow);

	return image;
}


void ui::DebugPix(Pix* image)
{
	Pix* gray = pixConvertRGBToGray(image, 0.f, 0.f, 0.f);
	Pix* threshold = pixScaleGray4xLIThresh(gray, 190);
	Pix* unpack = pixUnpackBinary(threshold, 8, 0);

	DebugOcr(image, "original");
	DebugOcr(threshold, "scale4-threshold");
	DebugOcr(unpack, "unpack");

	pixDestroy(&gray);
	pixDestroy(&threshold);
	pixDestroy(&unpack);
}

void ui::DebugOcr(Pix * image, const std::string& what)
{
	std::string filename = "capture-" + what + ".bmp";
	pixWrite(filename.c_str(), image, IFF_BMP);

	_tesseractAPI->SetImage(image);
	char* outText = _tesseractAPI->GetUTF8Text();
	//	std::cout << what << " : " << outText << std::endl;
	std::string trimmed = std::string(outText);
	boost::algorithm::trim(trimmed);
	std::cout << what << " : " << trimmed << std::endl;
	delete[] outText;
}

}

