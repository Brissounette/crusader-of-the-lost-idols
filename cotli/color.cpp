#include "color.h"

#include <algorithm>


namespace cotli {

namespace color {

bool operator==(const RGB& c1, const RGB& c2)
{
	return (c1.red == c2.red) && (c1.green == c2.green) && (c1.blue == c2.blue);
}

bool operator!=(const RGB& c1, const RGB& c2)
{
	return !(c1 == c2);
}

std::ostream & operator<<(std::ostream& os, const RGB& c)
{
	os << "rgb(" << c.red << "," << c.green << "," << c.blue << ")";
	return os;
}

// http://www.compuphase.com/cmetric.htm
bool are_similar(const RGB& c1, const RGB& c2, float threshold)
{
	int rmean = (c1.red + c2.red) / 2;
	int dr = c1.red - c2.red;
	int dg = c1.green - c2.green;
	int db = c1.blue - c2.blue;
	double distance = sqrt((((512 + rmean)*dr*dr) >> 8) + 4 * dg*dg + (((767 - rmean)*db*db) >> 8));
//	std::cout << c1 << "/" << c2 << " - distance : " << distance << std::endl;;
	return distance < threshold;
}

bool near_of_grey(const RGB& c)
{
	bool result = false;

	if ((c.red == c.blue) && (c.blue == c.green))
		result = true;
	else
	{
		auto min_max = std::minmax({ c.red,c.blue,c.green });
		if (min_max.second - min_max.first <= 10)
			result = true;
	}

	return result;
}

bool near_of_greenyellow(const RGB& c)
{
	bool result = false;

	HSV hsv_color = rgb2hsv(c);
	float angular_hue = hsv_color.hue * 360.f;

	if ((angular_hue > 70.f) && (angular_hue < 144.f))
	{
		if ((hsv_color.value > 0.5f) && (hsv_color.saturation > 0.3f))
		{
			result = true;
		}
	}

	return result;
}

std::ostream & operator<<(std::ostream & os, const HSV& c)
{
	os << "hsv(" << c.hue << "," << c.saturation << "," << c.value << ")";
	return os;
}

HSV rgb2hsv(const RGB& c)
{
	float r = c.red / 255.0f;
	float g = c.green / 255.0f;
	float b = c.blue / 255.0f;

	float h, s, v;
	float K = 0.0f;

	if (g < b)
	{
		std::swap(g, b);
		K = -1.0;
	}

	if (r < g)
	{
		std::swap(r, g);
		K = -2.0f / 6.0f - K;
	}

	float chroma = r - std::min(g, b);
	h = fabs(K + (g - b) / (6.0f * chroma + 1e-20f));
	s = chroma / (r + 1e-20f);
	v = r;

	return HSV{ h,s,v };
}

}

}
