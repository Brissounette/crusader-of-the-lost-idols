#include "types.h"

#include <iostream>


namespace cotli {

std::ostream& operator<<(std::ostream & os, ability_state state)
{
	if (state == ability_state::locked)
		os << "locked";
	else if (state == ability_state::not_activable)
		os << "not activable";
	else if (state == ability_state::activable)
		os << "activable";
	else // state == ability_state::active
		os << "active";

	return os;
}

}
