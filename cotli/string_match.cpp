#include "string_match.h"

#include <string>
#include <algorithm>
#include <numeric>


namespace cotli {

namespace string_match {

// https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#C.2B.2B
// http://stackoverflow.com/questions/15416798/how-can-i-adapt-the-levenshtein-distance-algorithm-to-limit-matches-to-a-single
distance_t levenshtein_distance(const std::string &s1, const std::string &s2)
{
	// To change the type this function manipulates and returns, change
	// the return type and the types of the two variables below.
	size_t s1len = s1.size();
	size_t s2len = s2.size();

	distance_t* column = new distance_t[s1len + 1];
	std::iota(column, column + s1len + 1, 0);

	for (size_t x = 1; x <= s2len; x++)
	{
		column[0] = x;
		size_t last_diagonal = x - 1;
		for (size_t y = 1; y <= s1len; y++)
		{
			distance_t old_diagonal = column[y];
			auto possibilities = {
				column[y] + 1,
				column[y - 1] + 1,
				last_diagonal + (s1[y - 1] == s2[x - 1] ? 0 : 1)
			};
			column[y] = std::min(possibilities);
			last_diagonal = old_diagonal;
		}
	}
	distance_t result = column[s1len];
	delete[] column;
	return result;
}

bool is_near_of(const std::string & s1, const std::string & s2, float threshold)
{
	distance_t distance = levenshtein_distance(s1, s2);
	size_t longest = std::max(s1.size(), s2.size());
	float ratio = (static_cast<float>(longest - distance)) / longest;

	return ratio >= threshold;
}

}

}