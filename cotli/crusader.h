#ifndef __COTLI_CRUSADER__
#define __COTLI_CRUSADER__

#include "types.h"

#include <string>
#include <array>


namespace cotli {

	class Crusader {
		public:
			static const uint8_t default_nb_abilities = 7;
	
		private:
			std::string		_name;
			index_t			_primaryIdx;
			uint8_t			_nbAbilities;
			index_t			_slotIdx;
			std::array<bool, default_nb_abilities> _abilities;
			uint32_t		_level;
			uint32_t		_maxLevel;


		public:
			Crusader(const std::string& name, index_t pIdx, unsigned char nbAbilities, index_t slotIdx, uint32_t max);
			Crusader(Crusader&& crusader);
			Crusader() = delete;
			Crusader& operator=(const Crusader&) = delete;

			bool is_active() const { return _level > 0; }
			index_t index() const { return _primaryIdx; }
			uint32_t level() const { return _level; }
			void level(uint32_t level) { _level = level; }
			uint32_t max_level() const { return _maxLevel; }
			index_t slot_index() const { return _slotIdx; }
			size_t abilities_count() const { return _nbAbilities; }
			void mark_ability_active(size_t abilityIdx) { _abilities[abilityIdx] = true; };
			bool is_ability_active(size_t abilityIdx) const { return _abilities[abilityIdx]; }
			const std::string& name() const { return _name; }
	};
}


#endif // !__COTLI_CRUSADER__
