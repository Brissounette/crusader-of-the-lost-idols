#ifndef __COTLI_UI__
#define __COTLI_UI__


#include "types.h"
#include "color.h"

#include <string>
#include <functional>

#include <Windows.h>

#include <leptonica\allheaders.h>
#include <tesseract\baseapi.h>


namespace cotli {

class ui {
	
	private:
		enum class visibility {
			lower,
			visible,
			higher
		};		

	private:
		int		_maxCoord;
		int		_systemX;
		int		_systemY;
		index_t	_lowestVisibleCrusader;
		index_t _lowestCampaignVisible;
		HWND	_window;
		tesseract::TessBaseAPI* _tesseractAPI;

	public:
		ui();
		ui(const ui&) = delete;
		ui(ui&&) = delete;
		ui& operator=(const ui&) = delete;
		~ui();


		bool foreground();
		bool is_in_game();
		bool is_event_period();
		void reset_crusader_selector();

		bool select_crusader_index(index_t idx);
		bool select_crusader(index_t idx, const std::string& name);
		bool is_swappable_crusader(index_t idx);
		bool swap_active_crusader(index_t idx, const std::string& name);

		bool slideLeft();
		bool slideRight();
		
		uint32_t level_up(index_t crusader_idx, uint32_t max);
		uint32_t retrieve_level(index_t crusaderIdx);
		ability_state retrieve_ability(index_t crusaderIdx, index_t abilityIdx);

		// action
		void clickOnLeftSlider();
		void clickOnRightSlider();
		void click_on_ability(index_t crusaderIdx, index_t abilityIdx);

		void putCrusaderOnField(index_t crusaderIdx, POINT& slotCoord);
		void free_slot(POINT& slotCoord);

		void disable_auto_progression();
		void enable_auto_progression();

		void reset_world(index_t crusader_idx, index_t ability_idx);
		void start_objective(index_t campaign_idx, index_t objective_idx);

		std::string extract_text(const RECT& area);
		Pix* take_screenshot(const RECT& rect);
		void DebugPix(Pix* image);
		void DebugOcr(Pix* image, const std::string& what);

	private:
		void toScreenCoords(POINT& pt);
		void move(POINT& pt);
		void click(POINT& pt);
		void offset_area(RECT& area, index_t crusader_idx);
		void offset_ability_area(RECT& area, index_t crusader_idx, index_t ability_idx);
		void offset_point(POINT& pt, index_t crusader_idx);
		void offset_ability_point(POINT& pt, index_t crusader_idx, index_t ability_idx);

		visibility get_crusader_visibility(index_t idx);
		visibility get_campaign_visibility(index_t idx, bool is_event_period);
	
		bool is_ability_locked(Pix* img);
		bool is_ability_not_activable(Pix* img);
		bool is_ability_activable(Pix* img);

		void switch_auto_progression();
		bool is_enable_auto_progression();

		bool is_slider_enable(const RECT& area);

		void unlock_campaign_selector(bool is_event_period);

		void level_up_25(POINT& point);
		void level_up_100(POINT& point);

		bool iterate_through_pixels(Pix* img, const std::function<bool(const color::RGB&)> predicate, float threshold);

		bool wait_text_present(const RECT& area, const std::string& expected);
};

}


#endif // !__COTLI_UI__

