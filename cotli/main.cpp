#include "game_manager.h"

#include <cstdlib>
#include <iostream>


int main(int, char**)
{
	cotli::GameManager gm("config.json", "user.json");
	// gm.debug();
	gm.run();

	return EXIT_SUCCESS;
}
