#ifndef __COTLI_COLOR__
#define __COTLI_COLOR__


#include <iostream>

namespace cotli {

namespace color {

// RGB -----------------------------------------------------------------------

struct RGB {
	int red;
	int green;
	int blue;
};

bool operator==(const RGB& c1, const RGB& c2);

bool operator!=(const RGB& c1, const RGB& c2);

std::ostream& operator<<(std::ostream& os, const RGB& c);

bool are_similar(const RGB& c1, const RGB& c2, float threshold);

bool near_of_grey(const RGB& c);

bool near_of_greenyellow(const RGB& c);

// HSV -----------------------------------------------------------------------

struct HSV {
	float hue;
	float saturation;
	float value;
};

std::ostream& operator<<(std::ostream& os, const HSV& c);

// ---------------------------------------------------------------------------

HSV rgb2hsv(const RGB& c);

}

}

#endif // !__COTLI_COLOR__

