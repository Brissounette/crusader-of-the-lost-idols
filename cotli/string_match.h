#ifndef __COTLI_STRING_MATCH__
#define __COTLI_STRING_MATCH__

#include <string>

namespace cotli {

namespace string_match {

typedef unsigned int distance_t;

distance_t levenshtein_distance(const std::string &s1, const std::string &s2);

bool is_near_of(const std::string &s1, const std::string &s2, float threshold);

}

}

#endif // !__COTLI_STRING_MATCH__
