#include "types.h"
#include "crusader.h"

namespace cotli {

Crusader::Crusader(const std::string & name, index_t pIdx, unsigned char nbAbilities, index_t slotIdx, uint32_t max)
	: _name(name), _primaryIdx(pIdx), _nbAbilities(nbAbilities), _slotIdx(slotIdx), _level(0), _maxLevel(max)
{
	_abilities.fill(false);
}

Crusader::Crusader(Crusader && crusader)
{
	_name = std::move(crusader._name);
	_abilities = std::move(crusader._abilities);

	_primaryIdx = crusader._primaryIdx;
	_nbAbilities = crusader._nbAbilities;
	_slotIdx = crusader._slotIdx;
	_level = crusader._level;
	_maxLevel = crusader._maxLevel;
}

}
