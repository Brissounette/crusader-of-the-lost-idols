#ifndef __COTLI_WINDOW__
#define __COTLI_WINDOW__

#include <Windows.h>

namespace cotli {

namespace window {
	
HWND RetrieveWindow(LPCTSTR windowName);
void ForceForegroundWindow(HWND hWnd);

}

}



#endif // !__COTLI_WINDOW__
