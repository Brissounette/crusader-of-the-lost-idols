#ifndef __COTLI_TYPES__
#define __COTLI_TYPES__

#include <iostream>

namespace cotli {

typedef size_t index_t;

enum class ability_state {
	locked,
	not_activable,
	activable,
	active
};

std::ostream& operator<<(std::ostream& os, ability_state state);

}


#endif // !__COTLI_TYPES__

