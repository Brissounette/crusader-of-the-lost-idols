#ifndef __COTLI_GAME_MANAGER__
#define __COTLI_GAME_MANAGER__


#include "types.h"
#include "crusader.h"
#include "ui.h"

#include <map>
#include <string>
#include <vector>
#include <functional>
#include <limits>

namespace cotli {

class GameManager {

	public:
		typedef std::pair<POINT,index_t> FieldSlot;
		static const index_t unused_slot = std::numeric_limits<index_t>::max();

	private:
		std::map<index_t, Crusader> _crusaders;
		std::vector<FieldSlot> _fieldSlots;
		ui _ui;

		uint32_t _idle_time = 120;
		uint32_t _nb_loop_before_reset = 5;
		index_t _reset_crusader_idx = 19;
		index_t _reset_ability_idx = 5;


	public:
		GameManager(const std::string& configFile, const std::string& userFile);
		GameManager() = delete;
		GameManager(const GameManager&) = delete;
		GameManager(GameManager&&) = delete;


		void run();
		void debug();

	private:
		void restore_game();
		void start_game();
		void play();
		void reset_world();

		bool is_reset_ability(index_t crusaderIdx, index_t abilityIdx);
};

}



#endif // !__COTLI_GAME_MANAGER__

