#include "window.h"

#include <Windows.h>
#include <Shlwapi.h>


namespace cotli {

namespace window {

HWND RetrieveWindow(LPCTSTR windowName)
{
	HWND retVal = NULL;

	// Get first window on desktop
	HWND firstwindow = FindWindowEx(NULL, NULL, NULL, NULL);
	HWND window = firstwindow;
	TCHAR windowtext[MAX_PATH];

	// We need to get the console title in case we
	// accidentally match the search word with it
	// instead of the intended target window.
	TCHAR consoletitle[MAX_PATH];
	GetConsoleTitle(consoletitle, MAX_PATH);

	while (1)
	{
		// Check window title for a match
		GetWindowText(window, windowtext, MAX_PATH);
		if (StrStr(windowtext, windowName) != NULL &&
			StrCmp(windowtext, consoletitle) != 0)
		{
			retVal = window;
			break;
		}

		// Get next window
		window = FindWindowEx(NULL, window, NULL, NULL);
		if (window == NULL || window == firstwindow)
		{
			retVal = NULL;
			break;
		}
	}

	return retVal;
}

// http://stackoverflow.com/questions/6228089/how-do-i-bring-an-unmanaged-application-window-to-front-and-make-it-the-active
void ForceForegroundWindow(HWND hWnd)
{
	DWORD a;
	LockSetForegroundWindow(LSFW_UNLOCK);
	AllowSetForegroundWindow(ASFW_ANY);

	HWND hWndForeground = GetForegroundWindow();

	if (hWndForeground != 0)
	{
		if (hWndForeground != hWnd)
		{
			DWORD thread1 = GetWindowThreadProcessId(hWndForeground, &a);
			DWORD thread2 = GetCurrentThreadId();

			if (thread1 != thread2)
			{
				AttachThreadInput(thread1, thread2, true);
				LockSetForegroundWindow(LSFW_UNLOCK);
				AllowSetForegroundWindow(ASFW_ANY);
				BringWindowToTop(hWnd);
				if (IsIconic(hWnd))
				{
					ShowWindow(hWnd, SW_SHOWNORMAL);
				}
				else
				{
					ShowWindow(hWnd, SW_SHOW);
				}
				SetFocus(hWnd);
				AttachThreadInput(thread1, thread2, false);
			}
			else
			{
				AttachThreadInput(thread1, thread2, true);
				LockSetForegroundWindow(LSFW_UNLOCK);
				AllowSetForegroundWindow(ASFW_ANY);
				BringWindowToTop(hWnd);
				SetForegroundWindow(hWnd);
				SetFocus(hWnd);
				AttachThreadInput(thread1, thread2, false);

			}
			if (IsIconic(hWnd))
			{
				AttachThreadInput(thread1, thread2, true);
				LockSetForegroundWindow(LSFW_UNLOCK);
				AllowSetForegroundWindow(ASFW_ANY);
				BringWindowToTop(hWnd);
				ShowWindow(hWnd, SW_SHOWNORMAL);
				SetFocus(hWnd);
				AttachThreadInput(thread1, thread2, false);
			}
			else
			{
				BringWindowToTop(hWnd);
				ShowWindow(hWnd, SW_SHOW);
			}
		}
		SetForegroundWindow(hWnd);
		SetFocus(hWnd);
	}
	else
	{
		DWORD thread1 = GetWindowThreadProcessId(hWndForeground, &a);
		DWORD thread2 = GetCurrentThreadId();
		try
		{
			AttachThreadInput(thread1, thread2, true);
		}
		catch (...)
		{

		}
		LockSetForegroundWindow(LSFW_UNLOCK);
		AllowSetForegroundWindow(ASFW_ANY);
		BringWindowToTop(hWnd);
		SetForegroundWindow(hWnd);

		ShowWindow(hWnd, SW_SHOW);
		SetFocus(hWnd);
		AttachThreadInput(thread1, thread2, false);
	}
}

}

}
