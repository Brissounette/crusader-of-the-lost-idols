#include "game_manager.h"
#include "types.h"
#include "string_match.h"

#include <map>
#include <string>
#include <vector>
#include <functional>
#include <fstream>
#include <sstream>

#include <iostream> // debug

#include <rapidjson\document.h>
#include <rapidjson\istreamwrapper.h>


namespace cotli {

namespace impl__ {

rapidjson::Document LoadJsonFromFile(const std::string& filePath)
{
	std::ifstream ifs(filePath);
	rapidjson::IStreamWrapper isw(ifs);

	rapidjson::Document doc;
	doc.ParseStream(isw);

	return doc;
}

const rapidjson::Value& FindCampaign(const rapidjson::Value& array, const rapidjson::Value& toFind)
{
	rapidjson::SizeType i = 0;
	for (; i < array.Size(); ++i)
	{
		const rapidjson::Value& tmp = array[i];

		rapidjson::Value::ConstMemberIterator it = tmp.FindMember(rapidjson::Value("name"));
		if (it == tmp.MemberEnd())
		{
			// TODO : throw exception - declared campaign without name
		}

		if ((*it).value == toFind)
		{
			break;
		}
	}

	if (i >= array.Size())
	{
		// TODO : throw exception - campaign not found
	}

	return array[i];
}

const rapidjson::Value& FindObjective(const rapidjson::Value& campaign, const rapidjson::Value& objective)
{
	rapidjson::Value::ConstMemberIterator cit = campaign.FindMember("objectives");
	if (cit == campaign.MemberEnd())
	{
		// TODO : throw excetpion - no objective defined for selected campaign
	}

	const rapidjson::Value& objectives = (*cit).value;

	rapidjson::SizeType i = 0;
	for (; i < objectives.Size(); ++i)
	{
		const rapidjson::Value& tmp = objectives[i];

		rapidjson::Value::ConstMemberIterator it = tmp.FindMember(rapidjson::Value("name"));
		if (it == tmp.MemberEnd())
		{
			// TODO : throw exception - declared objective without name
		}

		if ((*it).value == objective)
		{
			break;
		}
	}

	if (i >= objectives.Size())
	{
		// TODO : throw exception - objective not found
	}

	return objectives[i];
}

void FillCrusaders(std::map<index_t, Crusader>& map, const rapidjson::Value& crusaders, const rapidjson::Value& requested)
{
	for (rapidjson::SizeType i = 0; i < crusaders.Size(); ++i)
	{
		const rapidjson::Value& sublist = crusaders[i];

		for (rapidjson::SizeType j = 0; j < sublist.Size(); ++j)
		{
			const rapidjson::Value& crusader = sublist[j];
			if (!crusader.HasMember("name"))
			{
				// TODO : throw exception - missing name in config.json
			}

			for (rapidjson::Value::ConstMemberIterator it = requested.MemberBegin(); it != requested.MemberEnd(); ++it)
			{
				const rapidjson::Value& user_crusader = (*it).value;
				if (!user_crusader.HasMember("name"))
				{
					// TODO : throw exception - missing name in user.json
				}

				if (crusader["name"] == user_crusader["name"])
				{
					unsigned char abilities = Crusader::default_nb_abilities;
					if (crusader.HasMember("abilities"))
					{
						abilities = static_cast<unsigned char>(crusader["abilities"].GetUint());
					}

					index_t slotIdx = GameManager::unused_slot;
					std::istringstream iss((*it).name.GetString());
					iss >> slotIdx;

					uint32_t max_level = 200;
					if (user_crusader.HasMember("level"))
					{
						const rapidjson::Value& level_value = user_crusader["level"];
						if (level_value.IsString() && (user_crusader["level"] == "unlimited"))
						{
							max_level = std::numeric_limits<uint32_t>::max();
						}
						else if (level_value.IsUint())
						{
							max_level = level_value.GetUint();
						}
					}

					map.emplace(i,
						Crusader(crusader["name"].GetString(),
							static_cast<index_t>(i),
							abilities,
							slotIdx,
							max_level)
					);
					break;
				}
			}
		}
	}
}

void FillSlots(std::vector<GameManager::FieldSlot>& toFill, const rapidjson::Value& fieldSlots)
{
	toFill.resize(fieldSlots.MemberCount());
	for (rapidjson::SizeType i = 0; i < fieldSlots.Size(); ++i)
	{
		const rapidjson::Value& slot = fieldSlots[i];

		GameManager::FieldSlot& slot_to_fill = toFill[i];
		slot_to_fill.second = GameManager::unused_slot;
		slot_to_fill.first.x = slot[0].GetUint();
		slot_to_fill.first.y = slot[1].GetUint();
	}
}

}

GameManager::GameManager(const std::string & configFile, const std::string & userFile)
	: _ui()
{
	rapidjson::Document global_cfg = impl__::LoadJsonFromFile(configFile);
	rapidjson::Document user_cfg = impl__::LoadJsonFromFile(userFile);

	const rapidjson::Value& campaigns = global_cfg["campaigns"];
	const rapidjson::Value& crusaders = global_cfg["crusaders"];

	const rapidjson::Value& selectedCampaign = impl__::FindCampaign(campaigns, user_cfg["campaign"]);
	const rapidjson::Value& selectedObjective = impl__::FindObjective(selectedCampaign, user_cfg["objective"]);

	rapidjson::Value::ConstMemberIterator userCrusaders_it = user_cfg.FindMember(rapidjson::Value("crusaders"));
	if (userCrusaders_it == user_cfg.MemberEnd())
	{
		// TODO : throw exception - crusaders not defined
	}
	const rapidjson::Value& userCrusaders = (*userCrusaders_it).value;

	if (userCrusaders.MemberCount() > selectedObjective["slots"].MemberCount())
	{
		// TODO : throw exception - more crusader than allowed
	}

	impl__::FillCrusaders(_crusaders, crusaders, userCrusaders);
	impl__::FillSlots(_fieldSlots, selectedObjective["slots"]);
}


void GameManager::run()
{
	_ui.foreground();

	if (_ui.is_in_game())
	{
		restore_game();
	}
	else
	{
		start_game();
	}

	while (true)
	{
		play();
		reset_world();
		start_game();
	}
}

void GameManager::debug()
{
	_ui.foreground();

	bool inGame = _ui.is_in_game();
	std::cout << inGame << std::endl;

	Sleep(1000);
}

void GameManager::restore_game()
{
	_ui.disable_auto_progression();
	Sleep(1000); // avoid level transition
	// TODO : click on crusader tab
	_ui.reset_crusader_selector();

	// retrieve crusaders level & abilities
	for (auto it = _crusaders.begin(); it != _crusaders.end(); it++)
	{
		Crusader& c = it->second;
		if (_ui.select_crusader(c.index(), c.name()))
		{
			uint32_t level = _ui.retrieve_level(c.index());
			// TODO : handle ocr error
			std::cout << c.name() << " : " << level << std::endl;
			if (level > 0)
			{
				c.level(level);
				for (size_t i = 0; i < c.abilities_count(); ++i)
				{
					ability_state state = _ui.retrieve_ability(c.index(), i);
					std::cout << '\t' << i << " : " << state << std::endl;

					if (state == ability_state::active)
					{
						c.mark_ability_active(i);
					}
					else if (state == ability_state::locked)
					{
						break;
					}
				}
			}
		}
		else
		{
			std::cout << c.name() << " : " << "not selected" << std::endl;
		}
	}

	// put on field selected crusaders
	for (auto crit = _crusaders.crbegin(); crit != _crusaders.crend(); ++crit)
	{
		const Crusader& c = crit->second;
		if (c.is_active())
		{
			size_t slot_idx = c.slot_index();
			POINT slot_coord = _fieldSlots[slot_idx].first;
			_fieldSlots[slot_idx].second = c.index();
			_ui.putCrusaderOnField(c.index(), slot_coord);
		}
	}

	// remove all crusaders from field
	for (auto cit = _fieldSlots.cbegin(); cit != _fieldSlots.end(); ++cit)
	{
		if ((*cit).second == GameManager::unused_slot)
		{
			POINT slot_coord = std::get<0>(*cit);
			_ui.free_slot(slot_coord);
		}
	}
}

void GameManager::start_game()
{
	_ui.start_objective(0, 11);
}

void GameManager::play()
{
	uint32_t loop_without_upgrade = 0;

	do {
		_ui.foreground();
		_ui.disable_auto_progression();
		Sleep(1000); // avoid level transition
		_ui.reset_crusader_selector();

		++loop_without_upgrade;

		std::cout << "up abilities" << std::endl;
		// activate possible abilities
		for (auto rit = _crusaders.rbegin(); rit != _crusaders.rend(); ++rit)
		{
			Crusader& c = rit->second;
			if (c.is_active())
			{
				for (size_t i = 0; i < c.abilities_count(); ++i)
				{
					if (!c.is_ability_active(i) && !is_reset_ability(c.index(), i))
					{
						ability_state state = _ui.retrieve_ability(c.index(), i);

						if (state == ability_state::activable)
						{
							_ui.click_on_ability(c.index(), i);
							c.mark_ability_active(i);
							loop_without_upgrade = 0;
						}
						else if ((state == ability_state::not_activable) || (state == ability_state::locked))
						{
							break;
						}
					}
				}
			}
		}

		std::cout << "up crusaders" << std::endl;
		// level up crusader
		for (auto rit = _crusaders.rbegin(); rit != _crusaders.rend(); ++rit)
		{
			Crusader& c = rit->second;
			std::cout << c.name() << " - " << c.level() << "/" << c.max_level() << std::endl;
			if (c.level() < c.max_level())
			{
				uint32_t up = _ui.level_up(c.index(), c.max_level() - c.level());
				if (up > 0)
				{
					loop_without_upgrade = 0;
					if (!c.is_active())
					{
						size_t slot_idx = c.slot_index();
						POINT slot_coord = _fieldSlots[slot_idx].first;
						_fieldSlots[slot_idx].second = c.index();
						_ui.putCrusaderOnField(c.index(), slot_coord);
					}
					uint32_t new_level = c.level() + up;
					c.level(new_level);
				}
			}
		}

		if (loop_without_upgrade >= _nb_loop_before_reset)
		{
			// check if reset world is available
			ability_state state = _ui.retrieve_ability(_reset_crusader_idx, _reset_ability_idx);
			if (state == ability_state::activable)
				break;
		}

		std::cout << "idle" << std::endl;
		_ui.enable_auto_progression();
		Sleep(_idle_time * 1000);

	} while (true);
}

void GameManager::reset_world()
{
	// TODO : maximize levels


	_ui.reset_world(_reset_crusader_idx, _reset_ability_idx);
}

bool GameManager::is_reset_ability(index_t crusaderIdx, index_t abilityIdx)
{
	return (crusaderIdx == _reset_crusader_idx) && (abilityIdx == _reset_ability_idx);
}

}
